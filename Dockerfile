FROM debian:jessie

######################## NGINX

RUN apt-get update && apt-get -y install build-essential zlib1g-dev libpcre3 libpcre3-dev libbz2-dev libssl-dev tar unzip

ADD http://nginx.org/download/nginx-1.9.2.tar.gz /tmp/
RUN cd /tmp  && tar -xzf nginx-1.9.2.tar.gz

RUN cd /tmp/nginx-1.9.2 && \
    ./configure \
      --prefix=/etc/nginx \
      --sbin-path=/usr/bin \
      --with-http_mp4_module \
      --pid-path=/var/run/nginx.pid \
      --error-log-path=/var/log/nginx/error.log \
      --http-log-path=/var/log/nginx/access.log


RUN cd /tmp/nginx-1.9.2 && \
    make && \
    make install

######################## FFMPEG

RUN echo "deb http://ftp.de.debian.org/debian stable main contrib non-free" >> /etc/apt/sources.list

RUN apt-get update

RUN apt-get -y --force-yes install \
 autoconf \
 automake \
 build-essential \
 libass-dev \
 libfreetype6-dev \
 libsdl1.2-dev \
 libtheora-dev \
 libtool \
 libva-dev \
 libvdpau-dev \
 libvorbis-dev \
 libxcb1-dev \
 libxcb-shm0-dev \
 libxcb-xfixes0-dev \
 pkg-config \
 texi2html \
 zlib1g-dev \
 libvpx-dev \
 libmp3lame-dev \
 yasm

RUN apt-get -y --force-yes install git

RUN cd /tmp/ && \
 git clone git://source.ffmpeg.org/ffmpeg.git && \
 git clone git://git.videolan.org/x264.git && \
 git clone --depth 1 git://github.com/mstorsjo/fdk-aac.git

RUN cd /tmp/x264 && \
 ./configure --enable-static && \
 make && make install && ldconfig

RUN cd /tmp/fdk-aac && \
 autoreconf -fiv && \
 ./configure --disable-shared && \
 make && make install

RUN cd /tmp/ffmpeg && \
 ./configure \
   --pkg-config-flags="--static" \
   --extra-libs=-static \
   --extra-cflags=--static \
   --enable-static \
   --disable-shared \
   --enable-gpl \
   --enable-libfdk-aac \
   --enable-libmp3lame \
   --enable-libtheora \
   --enable-libvorbis \
   --enable-libvpx \
   --enable-libx264 \
   --enable-nonfree && \
 make && make install

##########################################################

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

VOLUME ["/storage"]

ADD nginx.conf /etc/nginx/conf/nginx.conf
ADD default.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]
